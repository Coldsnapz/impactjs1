ig.module(
	'game.main'
)
	.requires(
		'impact.game',
		'game.entities.background',
		'game.entities.player',
		'impact.font',
		'game.entities.enemy'
	)
	.defines(function () {

		MyGame = ig.Game.extend({

			// Load a font
			font: new ig.Font('media/04b03.font.png'),

			tileImage: new ig.Image("media/assets/n_sur.png"),

			bg: null,

			gravity: 1200,

			player: null,

			start:false,

			resetTimer:null,

			init: function () {
				// Initialize your game here; bind keys etc.
				this.bg = ig.game.spawnEntity(EntityBackground, 0, 0);
				this.player = ig.game.spawnEntity(EntityPlayer, 0, 0);
				ig.game.sortEntitiesDeferred();

				if (this.bg) { this.bg.setSpeed(800); }

				if (!this.start && this.player.onGround) {
					this.player.run();
					this.bg.setSpeed(this.player.currentVelX);
					this.start = true;
				}

				if(this.enemyTimer && this.enemyTimer.delta() > 5){
					var enemySizeY=105;			ig.game.spawnEntity(EntityEnemy,640,480-enemySizeY-this.tileImage.height);
									ig.game.sortEntitiesDeferred();
									this.enemyTimer.reset();
					}

					if(this.end && this.resetTimer == null){
						this.resetTimer = new ig.Timer();
					  }
					  if(this.resetTimer &&
						this.resetTimer.delta()>1){
						ig.system.setGame( MyGame );
						this.enemyTimer=null;
						this.resetTimer=null;
					  }					

				ig.input.bind(ig.KEY.UP_ARROW, 'jump');
				ig.input.bind(ig.KEY.W, 'jump');

			},

			update: function () {
				// Update all entities and backgroundMaps
				this.parent();
				if (ig.input.pressed('jump')) {
					console.log('jump')
				}

				// Add your own, additional update code here
			},

			draw: function () {
				// Draw all entities and backgroundMaps
				this.parent();


				// Add your own drawing code here
				var x = ig.system.width / 4,
					y = ig.system.height*2 / 3;

				this.font.draw('I am Speed!', x, y, ig.Font.ALIGN.CENTER);
			}
		});


		// Start the Game with 60fps, a resolution of 320x240, scaled
		// up by a factor of 2
		ig.main('#canvas', MyGame, 60, 640, 480, 1);

	});
